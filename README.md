### About the repository ###

This is a basic Vue.js application that lists todo items, add new items to todo list and remove items from the list.

It captures use of the following in Vue.js

* Components
* Styles
* Methods
* Data-binding
* Event handling
* Routing
* Making Http Requests to Remote Web Services

### How to set up? ###

* Git clone
* Install Dependencies
* Run serve and access the application
